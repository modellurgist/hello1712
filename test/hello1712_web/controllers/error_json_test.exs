defmodule Hello1712Web.ErrorJSONTest do
  use Hello1712Web.ConnCase, async: true

  test "renders 404" do
    assert Hello1712Web.ErrorJSON.render("404.json", %{}) == %{errors: %{detail: "Not Found"}}
  end

  test "renders 500" do
    assert Hello1712Web.ErrorJSON.render("500.json", %{}) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
