defmodule Hello1712.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Hello1712Web.Telemetry,
      Hello1712.Repo,
      {DNSCluster, query: Application.get_env(:hello1712, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Hello1712.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Hello1712.Finch},
      # Start a worker by calling: Hello1712.Worker.start_link(arg)
      # {Hello1712.Worker, arg},
      # Start to serve requests, typically the last entry
      Hello1712Web.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Hello1712.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    Hello1712Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
